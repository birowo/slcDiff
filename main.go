package main

import (
	"fmt"
)

func main() {
	var inserted, deleted []int
	oldData := []int{1, 12, 4, 3, 17, 89, 23, 54, 13, 34, 98, 32, 76, 90, 96, 11, 33, 78, 99}
	newData := []int{12, 15, 19, 18, 4, 3, 17, 89, 54, 13, 98, 32, 76, 90, 11, 33, 65, 31, 101}

	fmt.Println("== MAP ==")
	inserted, deleted = splitInsertedAndDeletedArrayMap(oldData, newData)
	fmt.Printf("Inserted Data : %v \nDeleted Data : %v \n", inserted, deleted)
}

func splitInsertedAndDeletedArrayMap[T comparable](oldData, newData []T) (inserted, deleted []T) {
	oldDataLen, newDataLen := len(oldData), len(newData)
	keys := make(map[T]bool, oldDataLen+newDataLen)
	for _, key := range oldData {
		keys[key] = false //false : old data
	}
	for _, key := range newData {
		if isNew, ok := keys[key]; ok {
			if !isNew {
				delete(keys, key) // remove old data item exist in new data
			}

		} else {
			keys[key] = true //true : new data
		}
	}
	deleted, inserted = make([]T, 0, oldDataLen), make([]T, 0, newDataLen)
	for key, isNew := range keys {
		if isNew {
			inserted = append(inserted, key)
		} else {
			deleted = append(deleted, key)
		}
	}
	return
}
